<?php

namespace App\Form;

use App\Entity\ABONNE;
use App\Entity\LIGNEABONNE;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LgAbonneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('LGABONNE_TARIF')
            ->add('CPTEUR_NUM')
            ->add('RC_CCPTEUR_CODE')
            ->add('amperage')
            ->add('coeff')
            ->add('lusage')
            ->add('LIGNE_ABONNE_ETATCOMPTEUR')
            ->add('LIGNE_ABONNE_ANCIENINDEX')
            ->add('LIGNE_ABONNE_NBRREMOIS')
         ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => LIGNEABONNE::class,
        ]);
    }
}
