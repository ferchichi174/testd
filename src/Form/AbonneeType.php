<?php

namespace App\Form;

use App\Entity\ABONNE;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AbonneeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ABONNE_NUMSEQ')
            ->add('ABONNE_CODEUR')
            ->add('ABONNE_REFCLT')
            ->add('ABONNE_DTEDITION',DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('ABONNE_DTRELEVE',DateType::class, [
        'widget' => 'single_text',
        'format' => 'yyyy-MM-dd',
    ])
            ->add('ZONE_CODE')
            ->add('ABONNE_POSITION')
            ->add('abonne_etat')
            ->add('abonne_adr')
            ->add('abonne_nom')
            ->add('abonne_usage')
            ->add('abonne_nomlocalite')
//            ->add('lIGNEABONNE')

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ABONNE::class,
        ]);
    }
}
