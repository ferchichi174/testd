<?php

namespace App\Repository;

use App\Entity\TARIFCOMPTAGE;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TARIFCOMPTAGE>
 *
 * @method TARIFCOMPTAGE|null find($id, $lockMode = null, $lockVersion = null)
 * @method TARIFCOMPTAGE|null findOneBy(array $criteria, array $orderBy = null)
 * @method TARIFCOMPTAGE[]    findAll()
 * @method TARIFCOMPTAGE[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TARIFCOMPTAGERepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TARIFCOMPTAGE::class);
    }

    public function save(TARIFCOMPTAGE $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TARIFCOMPTAGE $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return TARIFCOMPTAGE[] Returns an array of TARIFCOMPTAGE objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TARIFCOMPTAGE
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
