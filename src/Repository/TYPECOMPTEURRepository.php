<?php

namespace App\Repository;

use App\Entity\TYPECOMPTEUR;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TYPECOMPTEUR>
 *
 * @method TYPECOMPTEUR|null find($id, $lockMode = null, $lockVersion = null)
 * @method TYPECOMPTEUR|null findOneBy(array $criteria, array $orderBy = null)
 * @method TYPECOMPTEUR[]    findAll()
 * @method TYPECOMPTEUR[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TYPECOMPTEURRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TYPECOMPTEUR::class);
    }

    public function save(TYPECOMPTEUR $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TYPECOMPTEUR $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return TYPECOMPTEUR[] Returns an array of TYPECOMPTEUR objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TYPECOMPTEUR
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
