<?php

namespace App\Admin;




use App\Entity\Chef;
use App\Entity\TSP;
use App\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Route\RouteCollectionInterface;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ChefAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof Chef
            ? $object->getUsername()
            : 'Nom de chef'; // shown in the breadcrumb on the create view
    }
    protected function configureFormFields(FormMapper $form): void
    {
        $form->add('USER_NAME', TextType::class);
        $form->add('email', TextType::class);

    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {

        $datagrid->add('id');
        $datagrid->add('email');
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->add('id', TextType::class);
        $list->add('email', TextType::class);

        $list->add(ListMapper::NAME_ACTIONS, null, [
            'actions' => [
                'show' => [],
                'edit' => [],
                'delete' => [],
            ]]);

    }


    protected function configureShowFields(ShowMapper $show): void
    {

        $show

            ->add('id')
            ->add('email');


    }

}