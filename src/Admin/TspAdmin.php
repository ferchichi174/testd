<?php


namespace App\Admin;

use App\Entity\ABONNE;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class TspAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof ABONNE
            ? $object->getABONNENUMSEQ()
            : 'Blog Post'; // shown in the breadcrumb on the create view
    }


    protected function configureFormFields(FormMapper $form): void
    {
        $form
        ->add('TSP_REF', TextType::class)
        ->add('TSP_MARQUE', TextType::class)
        ->add('TSP_DTENTRY', DateType::class, [
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd',
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid->add('TSP_REF');
        $datagrid->add('TSP_MARQUE');
        $datagrid->add('TSP_DTENTRY');

    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->add('TSP_REF');
        $list->add('TSP_MARQUE');
        $list->add('TSP_DTENTRY');
        $list->add(ListMapper::NAME_ACTIONS, null, [
            'actions' => [
                'show' => [],
                'edit' => [],
                'delete' => [],
            ]]);
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show->add('id');
        $show->add('TSP_REF');
        $show->add('TSP_MARQUE');
        $show->add('TSP_DTENTRY');
    }
}