<?php


namespace App\Admin;

use App\Entity\ABONNE;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class DistAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof ABONNE
            ? $object->getDISTCODREG()
            : 'Blog Post'; // shown in the breadcrumb on the create view
    }



    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('DIST_CODREG')
            ->add('DIST_LIBELLEFR')
            ->add('DIST_LIBELLEAR')
            ->add('DIST_TEL')
            ->add('DIST_TOURNEEDEBUT')
            ->add('DIST_TOURNEEFIN')
            ->add('DIST_RIB')
            ->add('DIST_FAX')
            ->add('DIST_TDEBMTZ')
            ->add('DIST_VERSION')
            ->add('DIST_IDUTILISATEUR')
            ->add('DIST_TDEBMTZRF')
            ->add('DIST_TFINMT2RF')
            ->add('DIST_NUMAFFILIATION')
            ->add('DIST_TYPE')
            ->add('DIST_POLE')
            ->add('DIST_TELTECH')
            ->add('DIST_CODENEW');
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid
            ->add('DIST_CODREG')
            ->add('DIST_LIBELLEFR')
            ->add('DIST_LIBELLEAR')
            ->add('DIST_TEL')
            ->add('DIST_TOURNEEDEBUT')
            ->add('DIST_TOURNEEFIN')
            ->add('DIST_RIB')
            ->add('DIST_FAX')
            ->add('DIST_TDEBMTZ')
            ->add('DIST_VERSION')
            ->add('DIST_IDUTILISATEUR')
            ->add('DIST_TDEBMTZRF')
            ->add('DIST_TFINMT2RF')
            ->add('DIST_NUMAFFILIATION')
            ->add('DIST_TYPE')
            ->add('DIST_POLE')
            ->add('DIST_TELTECH')
            ->add('DIST_CODENEW');


    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('id')
            ->add('DIST_CODREG')
            ->add('DIST_LIBELLEFR')
            ->add('DIST_LIBELLEAR')
            ->add('DIST_TEL')
            ->add('DIST_TOURNEEDEBUT')
            ->add(ListMapper::NAME_ACTIONS, null, [
        'actions' => [
            'show' => [],
            'edit' => [],
            'delete' => [],
        ]]);



    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('DIST_CODREG')
            ->add('DIST_LIBELLEFR')
            ->add('DIST_LIBELLEAR')
            ->add('DIST_TEL')
            ->add('DIST_TOURNEEDEBUT')
            ->add('DIST_TOURNEEFIN')
            ->add('DIST_RIB')
            ->add('DIST_FAX')
            ->add('DIST_TDEBMTZ')
            ->add('DIST_VERSION')
            ->add('DIST_IDUTILISATEUR')
            ->add('DIST_TDEBMTZRF')
            ->add('DIST_TFINMT2RF')
            ->add('DIST_NUMAFFILIATION')
            ->add('DIST_TYPE')
            ->add('DIST_POLE')
            ->add('DIST_TELTECH')
            ->add('DIST_CODENEW');


    }
}