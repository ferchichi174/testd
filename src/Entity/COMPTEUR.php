<?php

namespace App\Entity;

use App\Repository\COMPTEURRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: COMPTEURRepository::class)]
class COMPTEUR
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`cpteur_id`')]
    private ?int $ID_CPTEUR = null;

    #[ORM\Column(length: 2)]
    private ?string $CF_AB_CODE = null;

    #[ORM\Column(length: 9)]
    private ?string $CF_AB_REF = null;

    #[ORM\Column]
    private ?int $RC_BTTARIF = null;

    #[ORM\Column(length: 14)]
    private ?string $CPTEUR_NUM = null;

    #[ORM\Column]
    private ?int $RC_CCPTEUR_CODE = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $CAPTEUR_DATEMES = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $CPTEUR_DATEPOS = null;

    #[ORM\Column]
    private ?int $CPTEUR_AVCONSO = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $CPTEUR_DATMAJ = null;

    #[ORM\Column(length: 3)]
    private ?string $CPTEUR_UFCODE = null;

    #[ORM\Column]
    private ?int $CPTEUR_IDUTILISATEUR = null;

    #[ORM\Column(length: 3)]
    private ?string $RC_BT_EMPCPTR_ACCES = null;

    #[ORM\Column(length: 4)]
    private ?string $RC_BT_EMPCPTR_CODE = null;

    #[ORM\Column(length: 20)]
    private ?string $RC_BT_MARQCPTR_CODE = null;

    #[ORM\Column(length: 2)]
    private ?string $RC_BT_TYPCPTR_CODE = null;

    #[ORM\Column]
    private ?int $RC_BT_TYPCPTR_TYPE = null;

    #[ORM\Column]
    private ?int $RC_BT_TYPCPTR_NB_TARIF = null;

    #[ORM\Column]
    private ?int $RC_BT_TYPCPTR_NB_TARIF1 = null;

    #[ORM\Column(length: 1)]
    private ?string $RC_BT_SCPTR_CODE = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $CPTEUR_DATEDEPOSE = null;

    #[ORM\Column]
    private ?float $CPTEUR_COEFF = null;

    #[ORM\Column]
    private ?int $CPTEUR_DINDEX = null;

    #[ORM\Column]
    private ?int $CPTEUR_LOCK = null;

    #[ORM\Column]
    private ?float $CPTEUR_PINSTALLEE = null;

    #[ORM\ManyToOne(inversedBy: 'COMPTEURS')]
    #[ORM\JoinColumn(name: 'post_id', referencedColumnName: 'post_id')]
    private ?POSTE $CODE_POSTE = null;

    public function getId(): ?int
    {
        return $this->ID_CPTEUR;
    }

    public function getCFABCODE(): ?string
    {
        return $this->CF_AB_CODE;
    }

    public function setCFABCODE(string $CF_AB_CODE): self
    {
        $this->CF_AB_CODE = $CF_AB_CODE;

        return $this;
    }

    public function getCFABREF(): ?string
    {
        return $this->CF_AB_REF;
    }

    public function setCFABREF(string $CF_AB_REF): self
    {
        $this->CF_AB_REF = $CF_AB_REF;

        return $this;
    }

    public function getRCBTTARIF(): ?int
    {
        return $this->RC_BTTARIF;
    }

    public function setRCBTTARIF(int $RC_BTTARIF): self
    {
        $this->RC_BTTARIF = $RC_BTTARIF;

        return $this;
    }

    public function getCPTEURNUM(): ?string
    {
        return $this->CPTEUR_NUM;
    }

    public function setCPTEURNUM(string $CPTEUR_NUM): self
    {
        $this->CPTEUR_NUM = $CPTEUR_NUM;

        return $this;
    }

    public function getRCCCPTEURCODE(): ?int
    {
        return $this->RC_CCPTEUR_CODE;
    }

    public function setRCCCPTEURCODE(int $RC_CCPTEUR_CODE): self
    {
        $this->RC_CCPTEUR_CODE = $RC_CCPTEUR_CODE;

        return $this;
    }

    public function getCAPTEURDATEMES(): ?\DateTimeInterface
    {
        return $this->CAPTEUR_DATEMES;
    }

    public function setCAPTEURDATEMES(\DateTimeInterface $CAPTEUR_DATEMES): self
    {
        $this->CAPTEUR_DATEMES = $CAPTEUR_DATEMES;

        return $this;
    }

    public function getCPTEURDATEPOS(): ?\DateTimeInterface
    {
        return $this->CPTEUR_DATEPOS;
    }

    public function setCPTEURDATEPOS(\DateTimeInterface $CPTEUR_DATEPOS): self
    {
        $this->CPTEUR_DATEPOS = $CPTEUR_DATEPOS;

        return $this;
    }

    public function getCPTEURAVCONSO(): ?int
    {
        return $this->CPTEUR_AVCONSO;
    }

    public function setCPTEURAVCONSO(int $CPTEUR_AVCONSO): self
    {
        $this->CPTEUR_AVCONSO = $CPTEUR_AVCONSO;

        return $this;
    }

    public function getCPTEURDATMAJ(): ?\DateTimeInterface
    {
        return $this->CPTEUR_DATMAJ;
    }

    public function setCPTEURDATMAJ(\DateTimeInterface $CPTEUR_DATMAJ): self
    {
        $this->CPTEUR_DATMAJ = $CPTEUR_DATMAJ;

        return $this;
    }

    public function getCPTEURUFCODE(): ?string
    {
        return $this->CPTEUR_UFCODE;
    }

    public function setCPTEURUFCODE(string $CPTEUR_UFCODE): self
    {
        $this->CPTEUR_UFCODE = $CPTEUR_UFCODE;

        return $this;
    }

    public function getCPTEURIDUTILISATEUR(): ?int
    {
        return $this->CPTEUR_IDUTILISATEUR;
    }

    public function setCPTEURIDUTILISATEUR(int $CPTEUR_IDUTILISATEUR): self
    {
        $this->CPTEUR_IDUTILISATEUR = $CPTEUR_IDUTILISATEUR;

        return $this;
    }

    public function getRCBTEMPCPTRACCES(): ?string
    {
        return $this->RC_BT_EMPCPTR_ACCES;
    }

    public function setRCBTEMPCPTRACCES(string $RC_BT_EMPCPTR_ACCES): self
    {
        $this->RC_BT_EMPCPTR_ACCES = $RC_BT_EMPCPTR_ACCES;

        return $this;
    }

    public function getRCBTEMPCPTRCODE(): ?string
    {
        return $this->RC_BT_EMPCPTR_CODE;
    }

    public function setRCBTEMPCPTRCODE(string $RC_BT_EMPCPTR_CODE): self
    {
        $this->RC_BT_EMPCPTR_CODE = $RC_BT_EMPCPTR_CODE;

        return $this;
    }

    public function getRCBTMARQCPTRCODE(): ?string
    {
        return $this->RC_BT_MARQCPTR_CODE;
    }

    public function setRCBTMARQCPTRCODE(string $RC_BT_MARQCPTR_CODE): self
    {
        $this->RC_BT_MARQCPTR_CODE = $RC_BT_MARQCPTR_CODE;

        return $this;
    }

    public function getRCBTTYPCPTRCODE(): ?string
    {
        return $this->RC_BT_TYPCPTR_CODE;
    }

    public function setRCBTTYPCPTRCODE(string $RC_BT_TYPCPTR_CODE): self
    {
        $this->RC_BT_TYPCPTR_CODE = $RC_BT_TYPCPTR_CODE;

        return $this;
    }

    public function getRCBTTYPCPTRTYPE(): ?int
    {
        return $this->RC_BT_TYPCPTR_TYPE;
    }

    public function setRCBTTYPCPTRTYPE(int $RC_BT_TYPCPTR_TYPE): self
    {
        $this->RC_BT_TYPCPTR_TYPE = $RC_BT_TYPCPTR_TYPE;

        return $this;
    }

    public function getRCBTTYPCPTRNBTARIF(): ?int
    {
        return $this->RC_BT_TYPCPTR_NB_TARIF;
    }

    public function setRCBTTYPCPTRNBTARIF(int $RC_BT_TYPCPTR_NB_TARIF): self
    {
        $this->RC_BT_TYPCPTR_NB_TARIF = $RC_BT_TYPCPTR_NB_TARIF;

        return $this;
    }

    public function getRCBTTYPCPTRNBTARIF1(): ?int
    {
        return $this->RC_BT_TYPCPTR_NB_TARIF1;
    }

    public function setRCBTTYPCPTRNBTARIF1(int $RC_BT_TYPCPTR_NB_TARIF1): self
    {
        $this->RC_BT_TYPCPTR_NB_TARIF1 = $RC_BT_TYPCPTR_NB_TARIF1;

        return $this;
    }

    public function getRCBTSCPTRCODE(): ?string
    {
        return $this->RC_BT_SCPTR_CODE;
    }

    public function setRCBTSCPTRCODE(string $RC_BT_SCPTR_CODE): self
    {
        $this->RC_BT_SCPTR_CODE = $RC_BT_SCPTR_CODE;

        return $this;
    }

    public function getCPTEURDATEDEPOSE(): ?\DateTimeInterface
    {
        return $this->CPTEUR_DATEDEPOSE;
    }

    public function setCPTEURDATEDEPOSE(\DateTimeInterface $CPTEUR_DATEDEPOSE): self
    {
        $this->CPTEUR_DATEDEPOSE = $CPTEUR_DATEDEPOSE;

        return $this;
    }

    public function getCPTEURCOEFF(): ?float
    {
        return $this->CPTEUR_COEFF;
    }

    public function setCPTEURCOEFF(float $CPTEUR_COEFF): self
    {
        $this->CPTEUR_COEFF = $CPTEUR_COEFF;

        return $this;
    }

    public function getCPTEURDINDEX(): ?int
    {
        return $this->CPTEUR_DINDEX;
    }

    public function setCPTEURDINDEX(int $CPTEUR_DINDEX): self
    {
        $this->CPTEUR_DINDEX = $CPTEUR_DINDEX;

        return $this;
    }

    public function getCPTEURLOCK(): ?int
    {
        return $this->CPTEUR_LOCK;
    }

    public function setCPTEURLOCK(int $CPTEUR_LOCK): self
    {
        $this->CPTEUR_LOCK = $CPTEUR_LOCK;

        return $this;
    }

    public function getCPTEURPINSTALLEE(): ?float
    {
        return $this->CPTEUR_PINSTALLEE;
    }

    public function setCPTEURPINSTALLEE(float $CPTEUR_PINSTALLEE): self
    {
        $this->CPTEUR_PINSTALLEE = $CPTEUR_PINSTALLEE;

        return $this;
    }

    public function getCODEPOSTE(): ?POSTE
    {
        return $this->CODE_POSTE;
    }

    public function setCODEPOSTE(?POSTE $CODE_POSTE): self
    {
        $this->CODE_POSTE = $CODE_POSTE;

        return $this;
    }
}
