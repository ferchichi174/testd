<?php

namespace App\Entity;

use App\Repository\ABONNERepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ABONNERepository::class)]
class ABONNE
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name:'`abonne_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    public ?string $ABONNE_NUMSEQ = null;

    #[ORM\Column(length: 255)]
    private ?string $ABONNE_CODEUR = null;

    #[ORM\Column(type: Types::DATE_MUTABLE,nullable: true)]
    private ?\DateTimeInterface $ABONNE_DTEDITION = null;

    #[ORM\Column(type: Types::DATE_MUTABLE,nullable: true)]
    private ?\DateTimeInterface $ABONNE_DTRELEVE = null;

    #[ORM\Column(length: 255)]
    private ?string $ABONNE_REFCLT = null;


    #[ORM\Column(length: 5)]
    private ?string $ZONE_CODE = null;

    #[ORM\Column(length: 255)]
    private ?string $ABONNE_POSITION = null;



    #[ORM\Column(length: 5, nullable: true)]
    private ?string $abonne_etat = null;

    #[ORM\Column(length: 25, nullable: true)]
    private ?string $abonne_adr = null;

    #[ORM\Column(length: 25, nullable: true)]
    private ?string $abonne_nom = null;

    #[ORM\Column(length: 5, nullable: true)]
    private ?string $abonne_usage = null;


    #[ORM\OneToOne(mappedBy: 'ABONNE', cascade: ['persist', 'remove'])]
    private ?LIGNEABONNE $lIGNEABONNE = null;

    #[ORM\ManyToOne(inversedBy: 'ABONNES')]
    #[ORM\JoinColumn(name: 'loc_id', referencedColumnName: 'loc_id')]
    private ?LOCALITE $LOC = null;

    #[ORM\Column(length: 45, nullable: true)]
    private ?string $ABONNE_NOMLOCALITE = null;



    public function __construct()
    {
        $this->ABONNES = new ArrayCollection();
    }
    public function __toString(){
        return $this->abonne_nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getABONNENUMSEQ(): ?string
    {
        return $this->ABONNE_NUMSEQ;
    }

    public function setABONNENUMSEQ(string $ABONNE_NUMSEQ): self
    {
        $this->ABONNE_NUMSEQ = $ABONNE_NUMSEQ;

        return $this;
    }

    public function getABONNECODEUR(): ?string
    {
        return $this->ABONNE_CODEUR;
    }

    public function setABONNECODEUR(string $ABONNE_CODEUR): self
    {
        $this->ABONNE_CODEUR = $ABONNE_CODEUR;

        return $this;
    }


    public function getABONNEDTEDITION(): ?\DateTimeInterface
    {
        return $this->ABONNE_DTEDITION;
    }

    public function setABONNEDTEDITION(\DateTimeInterface $ABONNE_DTEDITION): self
    {
        $this->ABONNE_DTEDITION = $ABONNE_DTEDITION;

        return $this;
    }

    public function getABONNEDTRELEVE(): ?\DateTimeInterface
    {
        return $this->ABONNE_DTRELEVE;
    }

    public function setABONNEDTRELEVE(\DateTimeInterface $ABONNE_DTRELEVE): self
    {
        $this->ABONNE_DTRELEVE = $ABONNE_DTRELEVE;

        return $this;
    }

    public function getABONNEREFCLT(): ?string
    {
        return $this->ABONNE_REFCLT;
    }

    public function setABONNEREFCLT(string $ABONNE_REFCLT): self
    {
        $this->ABONNE_REFCLT = $ABONNE_REFCLT;

        return $this;
    }



    public function getZONECODE(): ?string
    {
        return $this->ZONE_CODE;
    }

    public function setZONECODE(string $ZONE_CODE): self
    {
        $this->ZONE_CODE = $ZONE_CODE;

        return $this;
    }

    public function getABONNEPOSITION(): ?string
    {
        return $this->ABONNE_POSITION;
    }

    public function setABONNEPOSITION(string $ABONNE_POSITION): self
    {
        $this->ABONNE_POSITION = $ABONNE_POSITION;

        return $this;
    }



    public function getAbonneEtat(): ?string
    {
        return $this->abonne_etat;
    }

    public function setAbonneEtat(?string $abonne_etat): self
    {
        $this->abonne_etat = $abonne_etat;

        return $this;
    }

    public function getAbonneAdr(): ?string
    {
        return $this->abonne_adr;
    }

    public function setAbonneAdr(?string $abonne_adr): self
    {
        $this->abonne_adr = $abonne_adr;

        return $this;
    }

    public function getAbonneNom(): ?string
    {
        return $this->abonne_nom;
    }

    public function setAbonneNom(?string $abonne_nom): self
    {
        $this->abonne_nom = $abonne_nom;

        return $this;
    }

    public function getAbonneUsage(): ?string
    {
        return $this->abonne_usage;
    }

    public function setAbonneUsage(?string $abonne_usage): self
    {
        $this->abonne_usage = $abonne_usage;

        return $this;
    }

    public function getLIGNEABONNE(): ?LIGNEABONNE
    {
        return $this->lIGNEABONNE;
    }

    public function setLIGNEABONNE(?LIGNEABONNE $lIGNEABONNE): self
    {
        // unset the owning side of the relation if necessary
        if ($lIGNEABONNE === null && $this->lIGNEABONNE !== null) {
            $this->lIGNEABONNE->setABONNE(null);
        }

        // set the owning side of the relation if necessary
        if ($lIGNEABONNE !== null && $lIGNEABONNE->getABONNE() !== $this) {
            $lIGNEABONNE->setABONNE($this);
        }

        $this->lIGNEABONNE = $lIGNEABONNE;

        return $this;
    }

    public function getLOC(): ?LOCALITE
    {
        return $this->LOC;
    }

    public function setLOC(?LOCALITE $LOC): self
    {
        $this->LOC = $LOC;

        return $this;
    }

    public function getABONNENOMLOCALITE(): ?string
    {
        return $this->ABONNE_NOMLOCALITE;
    }

    public function setABONNENOMLOCALITE(?string $ABONNE_NOMLOCALITE): self
    {
        $this->ABONNE_NOMLOCALITE = $ABONNE_NOMLOCALITE;

        return $this;
    }











}
