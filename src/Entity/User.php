<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`user_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 16)]
    private ?string $USER_NAME = null;

    #[ORM\Column(length: 180, name:"user_email")]
    private ?string $email = null;

    #[ORM\Column(nullable:true)]
    private array $roles = [];

    /**
     * @var string The hashed password
     */

    #[ORM\Column()]
    private ?string $password = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $user_createtime = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $resetToken = null;

    #[ORM\OneToOne(inversedBy: 'user', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(name: 'role_id', referencedColumnName: 'role_id')]
    private ?ROLE $ROLE = null;

    #[ORM\ManyToOne(inversedBy: 'users')]
    #[ORM\JoinColumn(name: 'tsp_id', referencedColumnName: 'tsp_id')]
    private ?TSP $Tsp = null;





    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->USER_NAME;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function setUSERNAME(string $USER_NAME): self
    {
        $this->USER_NAME = $USER_NAME;

        return $this;
    }

    public function getUserCreatetime(): ?\DateTimeInterface
    {
        return $this->user_createtime;
    }

    public function setUserCreatetime(\DateTimeInterface $user_createtime): self
    {
        $this->user_createtime = $user_createtime;

        return $this;
    }

    public function getResetToken(): ?string
    {
        return $this->resetToken;
    }

    public function setResetToken(?string $resetToken): self
    {
        $this->resetToken = $resetToken;

        return $this;
    }

    public function getROLE(): ?ROLE
    {
        return $this->ROLE;
    }

    public function setROLE(?ROLE $ROLE): self
    {
        $this->ROLE = $ROLE;

        return $this;
    }

    public function getTsp(): ?TSP
    {
        return $this->Tsp;
    }

    public function setTsp(?TSP $Tsp): self
    {
        $this->Tsp = $Tsp;

        return $this;
    }
































}
