<?php

namespace App\Entity;

use App\Repository\TARIFCOMPTAGERepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TARIFCOMPTAGERepository::class)]
#[ORM\Table(name: '`TARIF_COMPTAGE`')]
class TARIFCOMPTAGE
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name:'`tarif_cpt_id`')]
    private ?int $id = null;

    #[ORM\Column(length: 2, nullable: true)]
    private ?string $TARIF = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTARIF(): ?string
    {
        return $this->TARIF;
    }

    public function setTARIF(?string $TARIF): self
    {
        $this->TARIF = $TARIF;

        return $this;
    }
}
