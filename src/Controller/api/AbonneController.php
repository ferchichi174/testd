<?php

namespace App\Controller\api;

use App\Entity\ABONNE;
use App\Form\AbonneeType;
use App\Repository\ABONNERepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AbonneController extends AbstractApiController
{
    #[Route('/api/abonne', name:'app_abonne',methods: ['GET'])]
    public function getAll(ABONNERepository $ABONNERepository): Response
    {
        $abonnes= $ABONNERepository->findAll();

        // Customize the JSON response
        $responseData = [];

        foreach ($abonnes as $abonne) {
            $responseData[] = [
                'id' => $abonne->getId(),
                'ABONNENUMSEQ' => $abonne->getABONNENUMSEQ(),
                'ABONNECODEUR' => $abonne->getABONNECODEUR(),
                'ABONNEDTEDITION' => $abonne->getABONNEDTEDITION(),
                'ABONNEDTRELEVE' => $abonne->getABONNEDTRELEVE(),
                'ABONNEREFCLT' => $abonne->getABONNEREFCLT(),
                'ZONECODE' => $abonne->getZONECODE(),
                'BONNEPOSITION' => $abonne->getABONNEPOSITION(),
                'AbonneEtat' => $abonne->getAbonneEtat(),
                'AbonneNom' => $abonne->getAbonneNom(),
                'AbonneUsage' => $abonne->getAbonneUsage(),
                'abonneNomlocalite' => $abonne->getABONNENOMLOCALITE(),



                // Add more fields as needed
            ];
        }
        return $this->respond($responseData);
    }

    #[Route('/api/abonnee/add', name: 'abonneeadd', methods: ['POST'])]
    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(AbonneeType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var ABONNE $customer */
        $Abonne = $form->getData();


        $this->getDoctrine()->getManager()->persist($Abonne);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($Abonne);
    }

    #[Route('/api/abonne/{id}', name: 'deleteabonne', methods: ['DELETE'])]
    public function deleteUser($id)
    {
        // Récupérer l'utilisateur à supprimer depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $abonnee = $entityManager->getRepository(ABONNE::class)->find($id);

        if (!$abonnee) {
            return $this->respond('abonnee non trouvé', Response::HTTP_NOT_FOUND);
        }

        // Supprimer l'utilisateur
        $entityManager->remove($abonnee);
        $entityManager->flush();
        return $this->respond('abonnee supprimé', Response::HTTP_OK);
    }



    #[Route('/api/abonne/{id}', name: 'showabonnebyid', methods: ['GET'])]
    public function showUser($id)
    {

        // Récupérer l'utilisateur à supprimer depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $abonnee = $entityManager->getRepository(ABONNE::class)->find($id);

        if (!$abonnee) {
            return $this->respond('abonnee non trouvé', Response::HTTP_NOT_FOUND);
        }

        $responseData[] = [
            'id' => $abonnee->getId(),
            'ABONNENUMSEQ' => $abonnee->getABONNENUMSEQ(),
            'ABONNECODEUR' => $abonnee->getABONNECODEUR(),
            'ABONNEDTEDITION' => $abonnee->getABONNEDTEDITION(),
            'ABONNEDTRELEVE' => $abonnee->getABONNEDTRELEVE(),
            'ABONNEREFCLT' => $abonnee->getABONNEREFCLT(),
            'ZONECODE' => $abonnee->getZONECODE(),
            'BONNEPOSITION' => $abonnee->getABONNEPOSITION(),
            'AbonneEtat' => $abonnee->getAbonneEtat(),
            'AbonneNom' => $abonnee->getAbonneNom(),
            'AbonneUsage' => $abonnee->getAbonneUsage(),
            'abonneNomlocalite' => $abonnee->getABONNENOMLOCALITE(),



            // Add more fields as needed
        ];

        return $this->respond($responseData, Response::HTTP_OK);
    }


    #[Route('/api/abonne/{id}', name: 'updateabonnee', methods: ["PUT", "PATCH"])]
    public function updateUser(Request $request, $id)
    {
        // Récupérer l'utilisateur à mettre à jour depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $abonnee = $entityManager->getRepository(ABONNE::class)->find($id);

        if (!$abonnee) {
            return $this->respond('abonnee non trouvé', Response::HTTP_NOT_FOUND);
        }

        // Récupérer les données de la requête
        $data = json_decode($request->getContent(), true);

        // Mettre à jour les propriétés de l'utilisateur
        $abonnee->setABONNENUMSEQ($data['ABONNE_NUMSEQ']);
        $abonnee->setABONNECODEUR($data['ABONNE_CODEUR']);
        $abonnee->setABONNEREFCLT($data['ABONNE_REFCLT']);
        $abonnee->setABONNEPOSITION($data['ABONNE_POSITION']);
        $abonnee->setZONECODE($data['ZONE_CODE']);
        $abonnee->setAbonneEtat($data['abonne_etat']);
        $abonnee->setAbonneAdr($data['abonne_adr']);
        $abonnee->setAbonneNom($data['abonne_nom']);
        $abonnee->setAbonneUsage($data['abonne_usage']);
        $abonnee->setABONNENOMLOCALITE($data['abonne_nomlocalite']);
        // Enregistrer les modifications dans la base de données
        $entityManager->flush();

        return $this->respond('abonnée mis à jour', Response::HTTP_OK);
    }
}
