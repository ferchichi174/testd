<?php

namespace App\Controller\api;

use App\Entity\COMPTEUR;
use App\Form\COMPTEURType;
use App\Repository\COMPTEURRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompteurController extends AbstractApiController
{
    #[Route('api/compteur', name: 'app_compt', methods: ['GET'])]
    public function getAll(COMPTEURRepository   $Comptepository): Response
    {
        $releves= $Comptepository->findAll();


        // Customize the JSON response
        $responseData = [];

        foreach ($releves as $releve) {
            $responseData[] = [
                'id' => $releve->getId(),
                'CF_AB_CODE' => $releve->getCFABCODE(),
                'CF_AB_REF' => $releve->getCFABREF(),
                'RC_BTTARIF' => $releve->getRCBTTARIF(),
                'CPTEUR_NUM' => $releve->getCPTEURNUM(),
                'RC_CCPTEUR_CODE' => $releve->getRCCCPTEURCODE(),
                'CAPTEUR_DATEMES' => $releve->getCAPTEURDATEMES(),
                'CPTEUR_DATEPOS' => $releve->getCPTEURDATEPOS(),
                'CPTEUR_AVCONSO' => $releve->getCPTEURAVCONSO(),
                'CPTEUR_DATMAJ' => $releve->getCPTEURDATMAJ(),
                'CPTEUR_UFCODE' => $releve->getCPTEURUFCODE(),
                'CPTEUR_IDUTILISATEUR' => $releve->getCPTEURIDUTILISATEUR(),
                'RC_BT_EMPCPTR_ACCES' => $releve->getRCBTEMPCPTRACCES(),
                'RC_BT_EMPCPTR_CODE' => $releve->getRCBTEMPCPTRCODE(),
                'RC_BT_MARQCPTR_CODE' => $releve->getRCBTMARQCPTRCODE(),
                'RC_BT_TYPCPTR_CODE' => $releve->getRCBTTYPCPTRCODE(),
                'RC_BT_TYPCPTR_TYPE' => $releve->getRCBTTYPCPTRTYPE(),
                'RC_BT_TYPCPTR_NB_TARIF' => $releve->getRCBTTYPCPTRNBTARIF(),
                'RC_BT_TYPCPTR_NB_TARIF1' => $releve->getRCBTTYPCPTRNBTARIF1(),
                'RC_BT_SCPTR_CODE' => $releve->getRCBTSCPTRCODE(),
                'CPTEUR_DATEDEPOSE' => $releve->getCPTEURDATEDEPOSE(),
                'CPTEUR_COEFF' => $releve->getCPTEURCOEFF(),
                'CPTEUR_DINDEX' => $releve->getCPTEURDINDEX(),
                'CPTEUR_LOCK' => $releve->getCPTEURLOCK(),
                'CPTEUR_PINSTALLEE' => $releve->getCPTEURPINSTALLEE(),
//                'user' => $tsp->getUsers()->g,


//                'ABONNE' => $abonne->getABONNE()->getId(),




                // Add more fields as needed
            ];
        }
        return $this->respond($responseData);
    }
    #[Route('/api/compteur/add', name: 'compteuradd', methods: ['POST'])]
    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(COMPTEURType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var COMPTEUR $customer */
        $COMPTEUR = $form->getData();


        $this->getDoctrine()->getManager()->persist($COMPTEUR);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($COMPTEUR);
    }
}
