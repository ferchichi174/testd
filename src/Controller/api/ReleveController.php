<?php

namespace App\Controller\api;

use App\Entity\RELEVE;
use App\Form\RELEVEType;
use App\Repository\RELEVERepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReleveController extends AbstractApiController
{


    #[Route('api/releve', name: 'app_releve', methods: ['GET'])]
    public function getAll(RELEVERepository  $tspepository): Response
    {
        $releves= $tspepository->findAll();


        // Customize the JSON response
        $responseData = [];

        foreach ($releves as $releve) {
            $responseData[] = [
                'id' => $releve->getId(),
                'PQ_DATTRAIT' => $releve->getPQDATTRAIT(),
                'DIST_CODE' => $releve->getDISTCODE(),
                'LOC_CODE' => $releve->getLOCCODE(),
                'PQ_NUM' => $releve->getPQNUM(),
                'ZONE_CODE' => $releve->getZONECODE(),
                'CPTRLV_REF' => $releve->getCPTRLVREF(),
                'CPTRLV_NUMCPTEUR' => $releve->getCPTRLVNUMCPTEUR(),
                'CPTRLV_TARIFCAPTAGE' => $releve->getCPTRLVTARIFCAPTAGE(),
                'CPTRLV_PUISSCPTEUR' => $releve->getCPTRLVPUISSCPTEUR(),
                'CPTRLV_AINDEX' => $releve->getCPTRLVAINDEX(),
                'CPTRLV_NINDEX' => $releve->getCPTRLVNINDEX(),
                'CPTRLV_NBRMOIS' => $releve->getCPTRLVNBRMOIS(),
                'CPTRLLV_COEFF' => $releve->getCPTRLLVCOEFF(),
                'CPTRLV_CONSMOY' => $releve->getCPTRLVCONSMOY(),
                'OBSERV_CODE' => $releve->getOBSERVCODE(),
                'ERLV_CODE' => $releve->getERLVCODE(),
                'ERR_CODE' => $releve->getERRCODE(),
                'CPTRLV_VALIDRELV' => $releve->getCPTRLVVALIDRELV(),
                'CPTRLV_VALIDCHEF' => $releve->getCPTRLVVALIDCHEF(),
                'CPTRLV_DATMAJ' => $releve->getCPTRLVDATMAJ(),
                'CPTRLV_UF_CODE' => $releve->getCPTRLVUFCODE(),
                'CPTRLV_IDUTILISATEUR' => $releve->getCPTRLVIDUTILISATEUR(),
                'EMPCPTR_ACCES' => $releve->getEMPCPTRACCES(),
                'EMPCPTR_CODE' => $releve->getEMPCPTRCODE(),
                'TYPCPTR_CODE' => $releve->getTYPCPTRCODE(),
                'TYPCPTR_NBTARIF' => $releve->getTYPCPTRNBTARIF(),
                'TYPCPTR_TARIF1' => $releve->getTYPCPTRTARIF1(),
//                'user' => $tsp->getUsers()->g,


//                'ABONNE' => $abonne->getABONNE()->getId(),




                // Add more fields as needed
            ];
        }
        return $this->respond($responseData);
    }

    #[Route('/api/releve/add', name: 'releveadd', methods: ['POST'])]
    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(RELEVEType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var RELEVE $customer */
        $tsp = $form->getData();


        $this->getDoctrine()->getManager()->persist($tsp);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($tsp);
    }

    #[Route('/api/releve/{id}', name: 'deletereleve', methods: ['DELETE'])]
    public function deletereleve($id)
    {
        // Récupérer l'utilisateur à supprimer depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $releve = $entityManager->getRepository(RELEVE::class)->find($id);

        if (!$releve) {
            return $this->respond('Releve non trouvé', Response::HTTP_NOT_FOUND);
        }

        // Supprimer l'utilisateur
        $entityManager->remove($releve);
        $entityManager->flush();
        return $this->respond('Releve supprimé', Response::HTTP_OK);
    }

    #[Route('/api/releve/{id}', name: 'showrelve', methods: ['GET'])]
    public function showReleve($id)
    {

        // Récupérer l'utilisateur à supprimer depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $releve = $entityManager->getRepository(RELEVE::class)->find($id);

        if (!$releve) {
            return $this->respond('releve non trouvé', Response::HTTP_NOT_FOUND);
        }

        $responseData[] = [
            'id' => $releve->getId(),
            'PQ_DATTRAIT' => $releve->getPQDATTRAIT(),
            'DIST_CODE' => $releve->getDISTCODE(),
            'LOC_CODE' => $releve->getLOCCODE(),
            'PQ_NUM' => $releve->getPQNUM(),
            'ZONE_CODE' => $releve->getZONECODE(),
            'CPTRLV_REF' => $releve->getCPTRLVREF(),
            'CPTRLV_NUMCPTEUR' => $releve->getCPTRLVNUMCPTEUR(),
            'CPTRLV_TARIFCAPTAGE' => $releve->getCPTRLVTARIFCAPTAGE(),
            'CPTRLV_PUISSCPTEUR' => $releve->getCPTRLVPUISSCPTEUR(),
            'CPTRLV_AINDEX' => $releve->getCPTRLVAINDEX(),
            'CPTRLV_NINDEX' => $releve->getCPTRLVNINDEX(),
            'CPTRLV_NBRMOIS' => $releve->getCPTRLVNBRMOIS(),
            'CPTRLLV_COEFF' => $releve->getCPTRLLVCOEFF(),
            'CPTRLV_CONSMOY' => $releve->getCPTRLVCONSMOY(),
            'OBSERV_CODE' => $releve->getOBSERVCODE(),
            'ERLV_CODE' => $releve->getERLVCODE(),
            'ERR_CODE' => $releve->getERRCODE(),
            'CPTRLV_VALIDRELV' => $releve->getCPTRLVVALIDRELV(),
            'CPTRLV_VALIDCHEF' => $releve->getCPTRLVVALIDCHEF(),
            'CPTRLV_DATMAJ' => $releve->getCPTRLVDATMAJ(),
            'CPTRLV_UF_CODE' => $releve->getCPTRLVUFCODE(),
            'CPTRLV_IDUTILISATEUR' => $releve->getCPTRLVIDUTILISATEUR(),
            'EMPCPTR_ACCES' => $releve->getEMPCPTRACCES(),
            'EMPCPTR_CODE' => $releve->getEMPCPTRCODE(),
            'TYPCPTR_CODE' => $releve->getTYPCPTRCODE(),
            'TYPCPTR_NBTARIF' => $releve->getTYPCPTRNBTARIF(),
            'TYPCPTR_TARIF1' => $releve->getTYPCPTRTARIF1(),





            // Add more fields as needed
        ];

        return $this->respond($responseData, Response::HTTP_OK);
    }

    #[Route('/api/releve/{id}', name: 'updatereleve', methods: ["PUT", "PATCH"])]
    public function updateReleve(Request $request, $id)
    {
        // Récupérer l'utilisateur à mettre à jour depuis la base de données
        $entityManager = $this->getDoctrine()->getManager();
        $releve = $entityManager->getRepository(RELEVE::class)->find($id);

        if (!$releve) {
            return $this->respond('abonnee non trouvé', Response::HTTP_NOT_FOUND);
        }

        // Récupérer les données de la requête
        $data = json_decode($request->getContent(), true);

        // Mettre à jour les propriétés de l'utilisateur
        $releve->setDISTCODE($data['DIST_CODE']);
        $releve->setLOCCODE($data['LOC_CODE']);
        $releve->setPQNUM($data['PQ_NUM']);
        $releve->setZONECODE($data['ZONE_CODE']);
        $releve->setCPTRLVREF($data['CPTRLV_REF']);
        $releve->setCPTRLVNUMCPTEUR($data['CPTRLV_NUMCPTEUR']);
        $releve->setCPTRLVTARIFCAPTAGE($data['CPTRLV_TARIFCAPTAGE']);
        $releve->setCPTRLVPUISSCPTEUR($data['CPTRLV_PUISSCPTEUR']);
        $releve->setCPTRLVAINDEX($data['CPTRLV_AINDEX']);
        $releve->setCPTRLVNINDEX($data['CPTRLV_NINDEX']);
        $releve->setCPTRLVNBRMOIS($data['CPTRLV_NBRMOIS']);
        $releve->setCPTRLLVCOEFF($data['CPTRLLV_COEFF']);
        $releve->setCPTRLVCONSMOY($data['CPTRLV_CONSMOY']);
        $releve->setOBSERVCODE($data['OBSERV_CODE']);
        $releve->setERLVCODE($data['ERLV_CODE']);
        $releve->setERRCODE($data['ERR_CODE']);
        $releve->setCPTRLVVALIDRELV($data['CPTRLV_VALIDRELV']);
        $releve->setCPTRLVVALIDCHEF($data['CPTRLV_VALIDCHEF']);
        $releve->setCPTRLVUFCODE($data['CPTRLV_UF_CODE']);
        $releve->setCPTRLVIDUTILISATEUR($data['CPTRLV_IDUTILISATEUR']);
        $releve->setEMPCPTRACCES($data['EMPCPTR_ACCES']);
        $releve->setEMPCPTRCODE($data['EMPCPTR_CODE']);
        $releve->setTYPCPTRCODE($data['TYPCPTR_CODE']);
        $releve->setTYPCPTRTYPE($data['TYPCPTR_TYPE']);
        $releve->setTYPCPTRNBTARIF($data['TYPCPTR_NBTARIF']);
        $releve->setTYPCPTRTARIF1($data['TYPCPTR_TARIF1']);

        // Enregistrer les modifications dans la base de données
        $entityManager->flush();

        return $this->respond('Releve mis à jour', Response::HTTP_OK);
    }
}
