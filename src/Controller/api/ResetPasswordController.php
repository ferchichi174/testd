<?php

namespace App\Controller\api;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\InvalidArgumentException;

use Symfony\Component\Security\Core\User\UserProviderInterface;

use Symfony\Component\Validator\Validator\ValidatorInterface;
class ResetPasswordController extends AbstractController
{

    private $userProvider;
    private $passwordEncoder;
    private $validator;
    private $mailer;

    public function __construct(UserProviderInterface $userProvider, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator, \Swift_Mailer $mailer)
    {
        $this->userProvider = $userProvider;;
        $this->passwordEncoder = $passwordEncoder;
        $this->validator = $validator;
        $this->mailer = $mailer;
    }

    public function request(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        // Validation des données





        $user = $this->userProvider->loadUserByUsername($data['email']);

        if (!$user) {
            // L'utilisateur n'existe pas
            return new Response('User not found', Response::HTTP_NOT_FOUND);
        }

        // Générer le token de réinitialisation du mot de passe
        $resetToken = bin2hex(random_bytes(32));
        $user->setResetToken($resetToken);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        // Envoyer l'e-mail avec le lien de réinitialisation du mot de passe
        $resetUrl = $this->generateUrl('app_reset_password_reset', ['token' => $resetToken], UrlGeneratorInterface::ABSOLUTE_URL);
        $message = (new \Swift_Message('Réinitialisation du mot de passe'))
            ->setFrom('noreply@example.com')
            ->setTo($user->getEmail())
            ->setBody(
                "Pour réinitialiser votre mot de passe, veuillez cliquer sur le lien suivant : " . $resetUrl
            );
        $this->mailer->send($message);

        return new Response('Password reset email sent', Response::HTTP_OK);
    }

    public function reset(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        // Validation des données





        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['resetToken' => $data['token']]);

        if (!$user) {
            // Token invalide
            return new Response('Invalid token', Response::HTTP_BAD_REQUEST);
        }

        // Réinitialiser le mot de passe de l'utilisateur
        $user->setPassword($this->passwordEncoder->encodePassword($user, $data['password']));
        $user->setResetToken(null);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return new Response('Password reset successfully', Response::HTTP_OK);
    }
}
